# useCounter Hook

examples:
```
    const {counter, increment, decrement, reset} = iuserCounter(10);
```

useCounter() // expected a default value
