# useForm

Example:
```
    initialForm = {
        name: '',
        age: 0
    }
    const [values, handleInputChange, reset] = useForm(initialForm);
```